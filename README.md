# OpenML dataset: Eighty-years-of-Canadian-climate-data

https://www.openml.org/d/43843

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset has been compiled from public sources. The dataset consists of daily temperatures and precipitation from 13 Canadian centres. Precipitation is either rain or snow (likely snow in winter months). In 1940, there is daily data for seven out of the 13 centres, but by 1960 there is daily data from all 13 centres, with the occasional missing value.
Few of Canadas weather stations have been operating continuously, so we did need to patch together the data. Our source data is from https://climate-change.canada.ca/climate-data//daily-climate-data and here are the weather stations that we queried:
CALGARY INTL A
CALGARY INT'L A
EDMONTON INTL A
EDMONTON INT'L A
HALIFAX STANFIELD INT'L A
HALIFAX STANFIELD INT'L A
MONCTON A
MONCTON A
MONTREAL/PIERRE ELLIOTT TRUDEAU INTL
MONTREAL/PIERRE ELLIOTT TRUDEAU INTL A
OTTAWA INTL A
OTTAWA MACDONALD-CARTIER INT'L A
QUEBEC/JEAN LESAGE INTL
QUEBEC/JEAN LESAGE INTL A
SASKATOON DIEFENBAKER INT'L A
SASKATOON INTL A
ST JOHN'S A
ST JOHNS WEST CLIMATE
TORONTO INTL A
TORONTO LESTER B. PEARSON INT'L A
VANCOUVER INTL A
VANCOUVER INT'L A
WHITEHORSE A
WHITEHORSE A
WINNIPEG RICHARDSON INT'L A
WINNIPEG THE FORKS
Suggested uses: The data is suitable for time series forecasting.  At Penny Analytics, we are using this dataset to demonstrate outlier detection (anomaly detection) in multiple time series and here is the relevant blogpost: https://pennyanalytics.com/2020/01/28/climate-change-canadian-weather-anomalies-are-now-warmer-and-wetter/

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43843) of an [OpenML dataset](https://www.openml.org/d/43843). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43843/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43843/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43843/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

